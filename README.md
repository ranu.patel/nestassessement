# NestJS Form API

This is a sample NestJS API that provides three endpoints for managing form data.

## Getting Started

To get started with the project, follow these steps:

### Prerequisites

Make sure you have the following software installed on your machine:

- Node.js and npm

### Installation

1. Clone the repository:
  ```bash
   https://gitlab.com/ranu.patel/nestassessement.git
  ```

2. Navigate to the project directory:
  ```bash
  cd nest-assessment
  ```

3. Install dependencies
  ```bash
  npm install
  ```


### Running the Application
To run the application in development mode, use the following command:
```bash
npm run start:dev
```

### Usage
Once the application is running, you can access the API endpoints using tools like Postman.

Endpoints
* POST /forms: Create a new form entry.
{
  "uniqueid": "your-uuid-here",
  "title": "user",
  "name": "string",
  "email": "email",
  "phonenumber": "number",
  "isgraduate": "boolean"
}

* GET /forms?title=user.
{
  "uniqueid": "your-uuid-here",
  "name": "string",
  "email": "email",
  "phonenumber": "number",
  "isgraduate": "isgraduate"
}

* POST /forms/fill_data?title=user: Create a new data entry for form.
