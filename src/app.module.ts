import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FDForm } from './Form/fd-form.entity';
import { FDFormField } from './Form/fd-form-field.entity';
import { FDFormController } from './Form/fd-form.controller';
import { FDFormService } from './Form/fd-form.service';
import { FDFormFillData } from './Form/fd-form-fill-data.entity';
import { FDFormFillDataService } from './Form/fd-form-fill-data.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: '10.102.32.196',
      port: 3306,
      username: 'development',
      password: 'Admin@123',
      database: 'test',
      entities: [FDForm, FDFormField, FDFormFillData],
      synchronize: false,
    }),
    TypeOrmModule.forFeature([FDForm, FDFormField, FDFormFillData]),
  ],
  controllers: [AppController, FDFormController],
  providers: [AppService, FDFormService, FDFormFillDataService],
})
export class AppModule {}
