// fd-form.entity.ts
import { Entity, PrimaryColumn, Column, OneToMany } from 'typeorm';
import { FDFormField } from './fd-form-field.entity';

@Entity()
export class FDForm {
  @PrimaryColumn('uuid')
  id: string;

  @Column()
  title: string;

  @OneToMany(() => FDFormField, (field) => field.formid)
  fields: FDFormField[];
}
