import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { FDForm } from './fd-form.entity';

@Entity()
export class FDFormField {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 255 })
  formid: string;

  @Column({ length: 255 })
  fieldname: string;

  @Column({ length: 255 })
  fieldtype: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP(6)' })
  createdat: Date;

  @Column({
    type: 'datetime',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updatedat: Date;
}
