// fd-form.service.ts
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FDForm } from './fd-form.entity';
import { FDFormField } from './fd-form-field.entity';
import { CreateFDFormDto } from './create-fd-form.dto';
import { FDFormFillData } from './fd-form-fill-data.entity';

@Injectable()
export class FDFormService {
  constructor(
    @InjectRepository(FDForm)
    private fdFormRepository: Repository<FDForm>,
    @InjectRepository(FDFormField)
    private fdFormFieldRepository: Repository<FDFormField>,
  ) {}

  async createForm(id: string, title: string): Promise<FDForm> {
    const form = new FDForm();
    form.id = id;
    form.title = title;
    return await this.fdFormRepository.save(form);
  }

  async createFormField(
    formId1: string,
    fieldName: string,
    fieldType: string,
  ): Promise<FDFormField> {
    console.log('test');
    const formField = new FDFormField();
    formField.formid = formId1;
    formField.fieldname = fieldName;
    formField.fieldtype = fieldType;
    return await this.fdFormFieldRepository.save(formField);
  }
  async createFormAndFields(createFDFormDto: CreateFDFormDto): Promise<FDForm> {
    // Extract form id and title from DTO
    const { uniqueid, title } = createFDFormDto;

    // Create the form
    const form = await this.fdFormRepository.save({ id: uniqueid, title });

    // Extract dynamic field values from DTO
    const dynamicFields = Object.entries(createFDFormDto)
      .filter(
        ([key, value]) =>
          key !== 'uniqueid' && key !== 'title' && value !== undefined,
      )
      .map(([key, value]) => ({ fieldName: key, fieldValue: value }));

    // Create form fields dynamically
    const createdFields = await Promise.all(
      dynamicFields.map((field) => {
        return this.fdFormFieldRepository.save({
          formid: uniqueid,
          fieldname: field.fieldName,
          fieldtype: field.fieldValue,
        });
      }),
    );

    return form;
  }
  async findByTitle(title: string): Promise<FDForm> {
    return await this.fdFormRepository.findOneByOrFail({ title });
  }
  async findFormFieldIdByUniqueIdAndFieldName(
    uniqueId: string,
    fieldName: string,
  ): Promise<number> {
    const formField = await this.fdFormFieldRepository.findOne({
      where: { formid: uniqueId, fieldname: fieldName },
    });
    if (!formField) {
      throw new NotFoundException('Form field not found', fieldName);
    }
    return formField.id;
  }
  async getFormDataByTitle(paramTitle: string): Promise<any> {
    const result = await this.fdFormRepository
      .createQueryBuilder('fm')
      .innerJoin(FDFormField, 'fc', 'fm.id = fc.formid')
      .innerJoin(FDFormFillData, 'fv', 'fc.id = fv.form_field_id')
      .select('fm.id,fm.title,fc.fieldname,fc.fieldtype,fv.value')
      .where('fm.title = :paramTitle', { paramTitle })
      .getRawMany();
    return result;
  }

}
