import {
  Controller,
  Post,
  Body,
  NotFoundException,
  Query,
  Get,
} from '@nestjs/common';
import { FDFormService } from './fd-form.service';
import { CreateFDFormDto } from './create-fd-form.dto';
import { FDFormFillDataService } from './fd-form-fill-data.service';

@Controller('forms')
export class FDFormController {
  constructor(
    private readonly fdFormService: FDFormService,
    private readonly fdFormFillDataService: FDFormFillDataService,
  ) {}

  @Post()
  async createFormAndField(@Body() createFDFormDto: CreateFDFormDto) {
    const form = await this.fdFormService.createFormAndFields(createFDFormDto);
    return form;
  }

  @Post('/fill_data')
  async createFormData(@Body() formData: any, @Query('title') title: string) {
    if (!formData.hasOwnProperty('uniqueid')) {
      throw new NotFoundException('UUID field not found in JSON data');
    }
    const form = await this.fdFormService.findByTitle(title);
    if (!form) {
      throw new NotFoundException('Form not found');
    }
    const formEntryId: number = 1;
    const fieldsToSave = ['name', 'email', 'phonenumber', 'isgraduate'];

    for (const fieldName of fieldsToSave) {
      if (formData.hasOwnProperty(fieldName)) {
        const formFieldId =
          await this.fdFormService.findFormFieldIdByUniqueIdAndFieldName(
            formData.uniqueid,
            fieldName,
          );
        await this.fdFormFillDataService.createFormData(
          formEntryId,
          formFieldId,
          formData[fieldName],
        );
      }
    }

    return 'Form data added successfully';
  }
  @Get()
  async getFormDataByTitle(@Query('title') title: string) {
    if (!title) {
      throw new NotFoundException('Title query parameter is required');
    }

    const formData = await this.fdFormService.getFormDataByTitle(title);

    if (!formData) {
      throw new NotFoundException('Form data not found');
    }

    return formData;
  }
}
