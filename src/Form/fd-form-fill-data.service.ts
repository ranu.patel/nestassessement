// fd-form-fill-data.service.ts
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FDFormFillData } from './fd-form-fill-data.entity';

@Injectable()
export class FDFormFillDataService {
 
  constructor(
    @InjectRepository(FDFormFillData)
    private fdFormFillDataRepository: Repository<FDFormFillData>,
  ) {}

  async createFormData(
    formEntryId: number,
    formFieldId: number,
    value: string,
  ): Promise<FDFormFillData> {
    const formData = new FDFormFillData();
    formData.form_entry_id = formEntryId;
    formData.form_field_id = formFieldId;
    formData.value = value;
    return await this.fdFormFillDataRepository.save(formData);
  }
}
