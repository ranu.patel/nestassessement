export class CreateFDFormDto {
  uniqueid: string; // UUID for the form ID
  title: string;
  name: string;
  email: string;
  phonenumber: string;
  isgraduate: string;
}
