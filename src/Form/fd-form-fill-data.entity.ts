// fd-form-fill-data.entity.ts
import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class FDFormFillData {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  form_entry_id: number;

  @Column()
  form_field_id: number;

  @Column({ length: 255 })
  value: string;

  @Column({ type: 'datetime', default: () => 'CURRENT_TIMESTAMP(6)' })
  created_at: Date;

  @Column({
    type: 'datetime',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;
}
